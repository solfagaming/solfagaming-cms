<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (request()->session()->get('logged_in', false)) {
        return redirect('/admin/dashboard');
    }
    return view('layouts.login');
});
/* Auth Endpoints */
Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'AuthController@login')->name('login');
    Route::get('logout', 'AuthController@logout');
    // Route::post('register', 'AuthController@register');
    // Route::post('forgot-password', 'AuthController@forgotPassword');
});

Route::group(['prefix' => 'admin', 'middleware' => ['admin']], function () {
    Route::get('dashboard', 'DashboardController@index')->name('dashboard');

    // Route::group(['prefix' => 'banners'], function () {
    //     Route::get('/', 'BannerController@index');
    //     Route::get('datatables', 'BannerController@datatables');
    //     Route::get('create', 'BannerController@create');
    //     Route::post('store', 'BannerController@store');
    //     Route::get('{id}/edit', 'BannerController@edit');
    //     Route::post('update', 'BannerController@update');
    //     Route::post('destroy', 'BannerController@destroy');
    // });

    Route::group(['prefix' => 'brands'], function () {
        Route::get('/', 'BrandController@index');
        Route::get('datatables', 'BrandController@datatables');
        Route::get('create', 'BrandController@create');
        Route::post('store', 'BrandController@store');
        Route::get('{id}/edit', 'BrandController@edit');
        Route::post('update', 'BrandController@update');
        Route::post('destroy', 'BrandController@destroy');
    });

    Route::group(['prefix' => 'categories'], function () {
        Route::get('/', 'CategoryController@index');
        Route::get('datatables', 'CategoryController@datatables');
        Route::get('create', 'CategoryController@create');
        Route::post('store', 'CategoryController@store');
        Route::get('{id}/edit', 'CategoryController@edit');
        Route::post('update', 'CategoryController@update');
        Route::post('destroy', 'CategoryController@destroy');
    });

    Route::group(['prefix' => 'products'], function () {
        Route::get('/', 'ProductController@index');
        Route::get('datatables', 'ProductController@datatables');
        Route::get('create', 'ProductController@create');
        Route::post('store', 'ProductController@store');
        Route::get('{id}/edit', 'ProductController@edit');
        Route::post('update', 'ProductController@update');
        Route::post('destroy', 'ProductController@destroy');
        Route::group(['prefix' => 'images'], function () {
            Route::post('{id}/add', 'ProductController@addImage');
            Route::post('{id}/delete', 'ProductController@deleteImage');
        });
    });
});
