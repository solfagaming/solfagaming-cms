<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
        <div class="pull-left image">
            <img src="holder.js/160x160?auto=yes&theme=sky" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
            <p>{{ env('APP_NAME') }}</p>
            <a href="#"><i class="fa fa-circle text-success"></i> {{ session('user.name') }}</a>
        </div>
    </div>

    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="{!! classActivePath('admin/dashboard') !!}"><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
        <li class="treeview {!! classActiveSegment(2, 'brands') !!}">
            <a href="#">
                <i class="fa fa-dashboard"></i> <span>Brands</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li class="{!! classActivePath('admin/brands') !!}"><a href="{{ url('admin/brands') }}"><i class="fa fa-circle-o"></i> List Brands</a></li>
                <li class="{!! classActivePath('admin/brands/create') !!}"><a href="{{ url('admin/brands/create') }}"><i class="fa fa-circle-o"></i> Create Brand</a></li>
            </ul>
        </li>
        <li class="treeview {!! classActiveSegment(2, 'categories') !!}">
            <a href="#">
                <i class="fa fa-dashboard"></i> <span>Categories</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li class="{!! classActivePath('admin/categories') !!}"><a href="{{ url('admin/categories') }}"><i class="fa fa-circle-o"></i> List Categories</a></li>
                <li class="{!! classActivePath('admin/categories/create') !!}"><a href="{{ url('admin/categories/create') }}"><i class="fa fa-circle-o"></i> Create Category</a></li>
            </ul>
        </li>
        <li class="treeview {!! classActiveSegment(2, 'products') !!}">
            <a href="#">
                <i class="fa fa-cubes"></i> <span>Products</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li class="{!! classActivePath('admin/products') !!}"><a href="{{ url('admin/products') }}"><i class="fa fa-circle-o"></i> List Products</a></li>
                <li class="{!! classActivePath('admin/products/create') !!}"><a href="{{ url('admin/products/create') }}"><i class="fa fa-circle-o"></i> Create Product</a></li>
            </ul>
        </li>
    </ul>
</section>
<!-- /.sidebar -->
