@extends('layouts.admin')
@section('pagetitle', 'Edit Products')
@section('pagestyles')
    <link rel="stylesheet" href="{{ asset('js/plugins/dropzone/dropzone.min.css') }}">
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Products
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ url('admin/products') }}">Products</a></li>
            <li class="active">Edit Product</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#edit_product_content" data-toggle="tab">Edit Product</a></li>
              <li><a href="#product-images-form" data-toggle="tab">Product Images</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="edit_product_content">
                    <form class="" action="{{ url('admin/products/update') }}" method="post">
                        <input type="hidden" name="product_id" value="{{ $detail['product_id'] }}">
                        {{ csrf_field() }}
                        @if ($errors->has('message'))
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h4><i class="icon fa fa-ban"></i></h4>
                                {{ $errors->first('message') }}
                            </div>
                        @endif
                        @if(Session::has('flash_message'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ Session::get('flash_message') }}
                            </div>
                        @endif
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('brand_id') ? ' has-error' : '' }}">
                                        <label for="name">Brand</label>
                                        <select class="form-control" name="brand_id" id="brand_id">
                                            <option value="">Select Brand</option>
                                            @foreach ($brands as $brand)
                                                <option value="{{ $brand['brand_id'] }}"{{ old('brand_id', $detail['brand_id']) == $brand['brand_id'] ? ' selected' : '' }}>{{ $brand['name'] }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('brand_id'))
                                            <span class="help-block">
                                                {{ $errors->first('brand_id') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                                        <label for="name">Category</label>
                                        <select class="form-control" id="category_id" name="category_id" required>
                                            <option value="">Select Category</option>
                                            @foreach ($categories as $category)
                                                <option value="{{ $category['category_id'] }}"{{ old('category_id', $detail['category_id']) == $category['category_id'] ? ' selected' : '' }}>{{ $category['name'] }}</option>
                                                @foreach ($category['children'] as $child)
                                                    <option value="{{ $child['category_id'] }}"{{ old('category_id', $detail['category_id']) == $child['category_id'] ? ' selected' : '' }}>-- {{ $child['name'] }}</option>
                                                @endforeach
                                            @endforeach
                                        </select>
                                        @if ($errors->has('category_id'))
                                            <span class="help-block">
                                                {{ $errors->first('category_id') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('store_name') ? ' has-error' : '' }}">
                                        <label for="name">Product Name</label>
                                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $detail['name']) }}" placeholder="Enter name" required>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                {{ $errors->first('name') }}
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                        <label for="description">Description</label>
                                        <textarea name="description" class="form-control wysihtml5" id="description" placeholder="Enter description">{{ old('description', $detail['description']) }}</textarea>
                                        @if ($errors->has('description'))
                                            <span class="help-block">
                                                {{ $errors->first('description') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                                        <label for="price">Price</label>
                                        <input type="text" class="form-control" id="price" name="price" value="{{ old('price', $detail['price']) }}" placeholder="Enter Price" required>
                                        @if ($errors->has('price'))
                                            <span class="help-block">
                                                {{ $errors->first('price') }}
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('weight') ? ' has-error' : '' }}">
                                        <label for="weight">Weight</label>
                                        <input type="text" class="form-control" id="weight" name="weight" value="{{ old('weight', $detail['weight']) }}" placeholder="Enter Weight" required>
                                        @if ($errors->has('weight'))
                                            <span class="help-block">
                                                {{ $errors->first('weight') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('stock') ? ' has-error' : '' }}">
                                        <label for="stock">Stock</label>
                                        <input type="text" class="form-control" id="stock" name="stock" value="{{ old('stock', $detail['stock']) }}" placeholder="Enter Stock" required>
                                        @if ($errors->has('stock'))
                                            <span class="help-block">
                                                {{ $errors->first('stock') }}
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                                        <label for="state">States</label>
                                        <select class="form-control" id="state" name="state" required>
                                            <option value="">Select state</option>
                                            @foreach ($states as $key => $state)
                                                <option value="{{ $key }}"{{ old('state', $detail['state']) == $key ? ' selected' : '' }}>{{ $state }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('state'))
                                            <span class="help-block">
                                                {{ $errors->first('state') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer text-right">
                            <button type="submit" class="btn btn-success">Update</button>
                        </div>
                    </form>
                </div>
                <div class="tab-pane" id="product-images-form">
                    <form role="form" action="{{ url('admin/products/images/'.$detail['product_id'].'/add') }}" method="post" enctype="multipart/form-data" class="dropzone dropzone-file-area" id="images-form">
                        <input type="hidden" name="name" value="{{ $detail['name'] }}">
                        {{ csrf_field() }}
                        <h3 class="sbold">Drop files here or click to upload</h3>
                    </form>
                    <script type="text/javascript">
                        var productId = '{{ $detail['product_id'] }}';
                        var mockFiles = '<?php echo json_encode($detail['images']); ?>';
                        var asset_url = '{{ env('ASSET_URL') }}';
                    </script>
                    <style media="screen">
                        .dropzone .dz-preview .dz-image img{width:auto;height:100%;}
                    </style>
                </div>
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
@endsection
@section('pagescripts')
<script src="{{ asset('/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script src="{{ asset('/js/plugins/dropzone/dropzone.min.js') }}"></script>
<script src="{{ asset('/js/page/products.min.js') }}"></script>
@endsection