@extends('layouts.admin')
@section('pagetitle', 'Brands')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Brands
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Brands</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">List Brands</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body table-responsive">
                @if ($errors->has('message'))
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-ban"></i></h4>
                        {{ $errors->first('message') }}
                    </div>
                @endif
                @if(Session::has('flash_message'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('flash_message') }}
                    </div>
                @endif
                <div class="row">
                    <div class="col-sm-12 text-right">
                        <a href="{{ url('admin/brands/create') }}" class="btn btn-primary">Add Brand</a>
                    </div>
                </div>
                <br>
                <table id="branddatatable" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Slug</th>
                            <th>Description</th>
                            <th>State</th>
                            <th><i class="fa fa-fw fa-gear"></i></th>
                        </tr>
                    </thead>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
    <!-- Modal Dialog -->
    <div class="modal" id='modal_delete'>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Confirmation</h4>
                </div>
                <form action="{{ url('admin/brands/destroy') }}" method="post">
                    <input type="hidden" name="brand_id" value="" class="brand_id">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        Are you sure want to delete this brand?<br/>
                        <small class="text-red"><i class="fa fa-warning"></i> only database admin can restore deleted brand</small>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Yes, Delete <span class="itemname"></span>!</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection
@section('pagescripts')
<script src="{{ asset('/js/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('/js/page/brands.min.js') }}"></script>
@endsection