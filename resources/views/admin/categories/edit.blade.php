@extends('layouts.admin')
@section('pagetitle', 'Edit Categories')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Categories
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ url('admin/categories') }}">Categories</a></li>
            <li class="active">Edit Categories</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Edit Categories</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <form class="" action="{{ url('admin/categories/update') }}" method="post">
                <input type="hidden" name="category_id" value="{{ $detail['category_id'] }}">
                {{ csrf_field() }}
                @if ($errors->has('message'))
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-ban"></i></h4>
                        {{ $errors->first('message') }}
                    </div>
                @endif
                @if(Session::has('flash_message'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('flash_message') }}
                    </div>
                @endif
                <div class="box-body">
                    <div class="form-group{{ $errors->has('parent_id') ? ' has-error' : '' }}">
                        <label for="parent_id">Parent Catogory</label>
                        <select class="form-control" id="parent_id" name="parent_id" required>
                            <option value="0">No Parent</option>
                            @foreach ($parent_categories as $category)
                                <option value="{{ $category['category_id'] }}"{{ old('parent_id', $detail['parent_id']) == $category['category_id'] ? ' selected' : '' }}>{{ $category['name'] }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('parent_id'))
                            <span class="help-block">
                                {{ $errors->first('parent_id') }}
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('store_name') ? ' has-error' : '' }}">
                        <label for="name">Category Name</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $detail['name']) }}" placeholder="Enter name" required autofocus>
                        @if ($errors->has('name'))
                            <span class="help-block">
                                {{ $errors->first('name') }}
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                        <label for="description">Description</label>
                        <input type="text" class="form-control" id="description" name="description" value="{{ old('description', $detail['description']) }}" placeholder="Enter description" >
                        @if ($errors->has('description'))
                            <span class="help-block">
                                {{ $errors->first('description') }}
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('logo_url') ? ' has-error' : '' }}">
                        <label for="logo_url">Logo Url</label>
                        <input type="text" class="form-control" id="logo_url" name="logo_url" value="{{ old('logo_url', $detail['logo_url']) }}" placeholder="Enter logo URL" >
                        @if ($errors->has('logo_url'))
                            <span class="help-block">
                                {{ $errors->first('logo_url') }}
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                        <label for="state">States</label>
                        <select class="form-control" id="state" name="state" required>
                            <option value="">Select state</option>
                            @foreach ($states as $key => $state)
                                <option value="{{ $key }}"{{ old('state', $detail['state']) == $key ? ' selected' : '' }}>{{ $state }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('state'))
                            <span class="help-block">
                                {{ $errors->first('state') }}
                            </span>
                        @endif
                    </div>
                </div>
                <div class="box-footer text-right">
                    <button type="submit" class="btn btn-success">Update</button>
                </div>
            </form>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
@endsection
@section('pagescripts')
<script src="{{ asset('/js/page/categories.min.js') }}"></script>
@endsection