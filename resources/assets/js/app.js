
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('jquery');
window.$ = window.jQuery = require('jquery');
require('admin-lte/bootstrap/js/bootstrap');
require('admin-lte');
// import admin-lte plugins
require('admin-lte/plugins/iCheck/icheck');
require('admin-lte/plugins/slimScroll/jquery.slimscroll');
require('admin-lte/plugins/fastclick/fastclick');
require('admin-lte/plugins/datatables/jquery.dataTables.min');

//image holder
require('holderjs');

// window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example', require('./components/Example.vue'));
//
// const app = new Vue({
//     el: '#app'
// });
