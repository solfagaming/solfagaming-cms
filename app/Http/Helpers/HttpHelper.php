<?php

/**
 * Created by PhpStorm.
 * User: billyriantono
 * Date: 2/26/17
 * Time: 5:11 PM
 */
namespace App\Helpers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class HttpHelper
{
    var $baseUrl;

    /**
     * HttpHelper constructor.
     */
    public function __construct()
    {
        $this->baseUrl = getenv('API_URL_LOCAL');
        switch (strtolower(getenv('APP_ENV'))) {
            case "production":
                $this->baseUrl = getenv('API_URL');
                break;
            default:
                $this->baseUrl = getenv('API_URL_LOCAL');
                break;
        }
    }

    private function generateJsonPostData($data, $token = '')
    {
        $dataPost = json_encode($data, JSON_FORCE_OBJECT, 512);

        $headers = ['Content-Type' => 'application/json'];
        if (strlen($token) > 0) {
            $headers = ['Content-Type' => 'application/json', 'Authorization' => $token];
        }
        return ['debug' => false,
            'headers' => $headers,
            'body' => $dataPost
        ];
    }

    private function generateMultiPartPostData($data, $token = '')
    {
        // dd($data);
        $headers = [];
        $dataFormatted = $this->formattingMultipart($data);
        if (strlen($token) > 0) {
            $headers = ['Authorization' => $token];
        }
        return [
            'debug' => false,
            'headers' => $headers,
            'multipart' => $dataFormatted
        ];
    }

    private function formattingMultipart($data)
    {
        $dataFormmated = array();
        foreach ($data as $key => $value) {
            array_push($dataFormmated, [
                'Content-type' => 'multipart/form-data',
                'name' => $key,
                'contents' => $value
            ]);
        }
        return $dataFormmated;
    }

    private function generateQueryParameterData($data, $token = '')
    {
        $headers = [];
        if (strlen($token) > 0) {
            $headers = ['Authorization' => $token];
        }
        return [
            'debug' => false,
            'headers' => $headers,
            'query' => $data
        ];
    }

    /***
     * Generate Post Request
     * @param $url
     * @param $data
     */
    public function post($url, $data, $token = '', $isMultipart = false)
    {
        $url = $this->baseUrl . $url;
        $postData = $this->generateJsonPostData($data, $token);
        if ($isMultipart) {
            $postData = $this->generateMultiPartPostData($data, $token);
        }
        $client = $this->buildHttpClient($url, 'POST', $postData, $token);
        return $client;
    }

    public function get($url, $parameter, $token = '')
    {
        $url = $this->baseUrl . $url;
        $postData = $this->generateQueryParameterData($parameter, $token);
        $client = $this->buildHttpClient($url, 'GET', $postData, $token);
        return $client;
    }

    public function put($url, $data, $token = '')
    {
        $url = $this->baseUrl . $url;
        $postData = $this->generateJsonPostData($data, $token);
        $client = $this->buildHttpClient($url, 'PUT', $postData, $token);
        return $client;
    }

    private function buildHttpClient($url, $method, $data)
    {
        $client = new Client();
        try {
            $res = $client->request($method, $url, $data);
            return $res;
        } catch (RequestException $e) {
            return $e;
        }
    }
}
