<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\BadResponseException;

use DataTables;
use Image;
use Storage;
use Webpatser\Uuid\Uuid;
use Validator;

class ProductController extends Controller
{
    protected $states = [1 => 'ACTIVE', 2 => 'PENDING', 3 => 'BLOCKED', 4 => 'DELETED'];
    /**
     * Get order page.
     *
     * @param  Illuminate\Http\Request $request
     * @return Illuminate\Http\RedirectResponse
     */
    public function index(Request $request)
    {
        $this->userProfile = $this->getUserProfile($request);
        if (!$this->userProfile) {
            return redirect('auth/logout')->with('errors', 'Not Authorized.');
        }
        $data['user_profile'] = $this->userProfile;

        return view('admin.products.index', $data);
    }

    public function datatables(Request $request)
    {
        $this->userProfile = $this->getUserProfile($request);
        if (!$this->userProfile) {
            return redirect('auth/logout')->with('errors', 'Not Authorized.');
        }

        $token = $request->session()->get('auth.access_token', '');
        $params = [
            'get_all_state' => 1,
        ];
        $response = $this->httpHelper->post('admins/products/search', $params, 'Bearer ' . $token);
        if ($response instanceof BadResponseException) {
            return ['error' => true, 'message' => 'BadResponseException: ' . $response->getResponse()->getReasonPhrase()];
        }
        $json = json_decode($response->getBody(), true);
        if ($response->getStatusCode() == 200) {
            $products = collect($json['products']);
            // return $products;
            return DataTables::of($products)->toJson();
        } else {
            return $json;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->userProfile = $this->getUserProfile($request);
        if (!$this->userProfile) {
            return redirect('auth/logout')->with('errors', 'Not Authorized.');
        }
        $data['brands'] = $this->getBrands($request);
        $data['categories'] = $this->getCategories($request);
        $data['states'] = $this->states;
        return view('admin.products.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'name' => 'required',
            'weight' => 'required|numeric',
            'price' => 'required|numeric',
            'stock' => 'required|numeric',
            'state' => 'required|in:1,2,3,4',
            'product_images' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $images = $request->file('product_images');
        $imagesUploaded = [];
        foreach ($images as $key => $image) {
            $productName = $request->input('name');
            $dir = 'products/' . date('Ym');
            $options = [
                'visibility' => 'public',
                'ContentType' => $image->getMimeType(),
                'Expires' => 86400,
                'CacheControl' => 'public',
            ];
            $fileName =  str_slug($productName) . '-' . \Carbon\Carbon::now()->timestamp . $key . '.' . $image->getClientOriginalExtension();
            $path = $dir . '/' . $fileName;
            $imageMedium = Image::make($image)->widen(640, function ($constraint) {
                $constraint->upsize();
            })->stream();

            if (env('APP_ENV') == 'production') {
                Storage::disk('s3')->put($path, $imageMedium->__toString(), $options);
            } else {
                Storage::disk('local')->put($path, $imageMedium->__toString(), $options);
            }
            $itemImage = [
                'uuid' => Uuid::generate()->string,
                'title' => $productName,
                'photo' => $path
            ];
            array_push($imagesUploaded, $itemImage);
        }

        $token = $request->session()->get('auth.access_token', '');
        $params = [
            'brand_id' => $request->input('brand_id'),
            'category_id' => $request->input('category_id'),
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'price' => $request->input('price'),
            'weight' => $request->input('weight'),
            'stock' => $request->input('stock'),
            'state' => $request->input('state'),
            'images' => $imagesUploaded
        ];
        $response = $this->httpHelper->post('admins/products/add', $params, 'Bearer ' . $token);
        if ($response instanceof BadResponseException) {
            return redirect()->back()
                        ->withErrors(['message' => 'BadResponseException: ' . $response->getResponse()->getReasonPhrase()])
                        ->withInput();
        }
        $json = json_decode($response->getBody(), true);
        if ($response->getStatusCode() == 201) {
            $request->session()->flash('flash_message', 'Product created successfully.');
        } else {
            return redirect()->back()
                        ->withErrors(['message' => $json['message']])
                        ->withInput();
        }
        return redirect('admin/products');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $this->userProfile = $this->getUserProfile($request);
        if (!$this->userProfile) {
            return redirect('auth/logout')->with('errors', 'Not Authorized.');
        }
        $token = $request->session()->get('auth.access_token', '');
        $params = [
            'get_all_state' => 1,
            'product_id' => $id,
        ];
        $response = $this->httpHelper->post('admins/products/search', $params, 'Bearer ' . $token);
        if ($response instanceof BadResponseException) {
            $data = json_decode($response->getResponse()->getBody(), true);
            if (!empty($data)) {
                return redirect()->back()
                            ->withErrors(['message' => $data['message']])
                            ->withInput();
            } else {
                return redirect()->back()
                            ->withErrors(['message' => 'BadResponseException: ' . $response->getResponse()->getReasonPhrase()])
                            ->withInput();
            }
        }
        $json = json_decode($response->getBody(), true);
        if ($response->getStatusCode() == 200) {
            $data['detail'] = $json['products'][0];
        } else {
            return redirect()->back()
                        ->withErrors(['message' => $json['message']])
                        ->withInput();
        }
        $data['brands'] = $this->getBrands($request);
        $data['categories'] = $this->getCategories($request);
        $data['states'] = $this->states;
        return view('admin.products.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required|max:36',
            'name' => 'required|max:50',
            'description' => 'required|max:255',
            'logo_url' => 'nullable|url|max:255',
            'state' => 'required|in:1,2,3,4',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        $token = $request->session()->get('auth.access_token', '');
        $params = $request->all();
        $response = $this->httpHelper->post('admins/products/update', $params, 'Bearer ' . $token);
        if ($response instanceof BadResponseException) {
            $data = json_decode($response->getResponse()->getBody(), true);
            if (!empty($data)) {
                return redirect()->back()
                            ->withErrors(['message' => $data['message']])
                            ->withInput();
            } else {
                return redirect()->back()
                            ->withErrors(['message' => 'BadResponseException: ' . $response->getResponse()->getReasonPhrase()])
                            ->withInput();
            }
        }
        $json = json_decode($response->getBody(), true);
        if ($response->getStatusCode() == 200) {
            $request->session()->flash('flash_message', 'Product updated successfully.');
        } else {
            return redirect()->back()
                        ->withErrors(['message' => $json['message']])
                        ->withInput();
        }
        return redirect('admin/products/' . $params['product_id'] . '/edit');
    }

    public function addImage(Request $request, $id)
    {
        $this->userProfile = $this->getUserProfile($request);
        if (!$this->userProfile) {
            return response(['Unauthorized'], 401);
        }
        if ($request->hasFile('file')) {
            $image = $request->file('file');
            $productName = $request->input('name');
            $dir = 'products/' . date('Ym');
            $options = [
                'visibility' => 'public',
                'ContentType' => $image->getMimeType(),
                'Expires' => 86400,
                'CacheControl' => 'public',
            ];
            $fileName =  str_slug($productName) . '-' . \Carbon\Carbon::now()->timestamp . mt_rand(0, 100) . '.' . $image->getClientOriginalExtension();
            $path = $dir . '/' . $fileName;
            $imageMedium = Image::make($image)->widen(640, function ($constraint) {
                $constraint->upsize();
            })->stream();

            if (env('APP_ENV') == 'production') {
                Storage::disk('s3')->put($path, $imageMedium->__toString(), $options);
            } else {
                Storage::disk('local')->put($path, $imageMedium->__toString(), $options);
            }

            $token = $request->session()->get('auth.access_token', '');
            $params = [
                'product_id' => $id,
                'image_title' => $productName,
                'image_photo' => $path,
            ];
            $response = $this->httpHelper->post('admins/products/add-photo', $params, 'Bearer ' . $token);

            if ($response instanceof BadResponseException) {
                $json = json_decode($response->getResponse()->getBody(), true);
                return response()->json($json);
            }
            $json = json_decode($response->getBody(), true);
            if ($response->getStatusCode() != 200) {
                return response()->json($json, 404);
            }
            return response()->json($json['image']);
        } else {
            return response(['Image Not Found'], 404);
        }
    }

    public function deleteImage(Request $request, $productId)
    {
        $this->userProfile = $this->getUserProfile($request);
        if (!$this->userProfile) {
            return response(['Unauthorized'], 401);
        }

        $imageUuid = $request->input('image_uuid');
        $photo = $request->input('photo');

        $token = $request->session()->get('auth.access_token', '');
        $params = [
            'product_id' => $productId,
            'image_uuid' => $imageUuid,
        ];
        $response = $this->httpHelper->post('admins/products/delete-photo', $params, 'Bearer ' . $token);

        if ($response instanceof BadResponseException) {
            $json = json_decode($response->getResponse()->getBody(), true);
            return response()->json($json);
        }
        $json = json_decode($response->getBody(), true);
        if ($response->getStatusCode() != 200) {
            return response()->json($json);
        }
        if (env('APP_ENV') == 'production') {
            Storage::disk('s3')->delete($photo);
        } else {
            Storage::disk('local')->delete($photo);
        }
        return response()->json($json);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required|max:36',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        $token = $request->session()->get('auth.access_token', '');
        $params = $request->all();
        $response = $this->httpHelper->post('admins/products/delete', $params, 'Bearer ' . $token);
        if ($response instanceof BadResponseException) {
            $data = json_decode($response->getResponse()->getBody(), true);
            if (!empty($data)) {
                return redirect()->back()
                            ->withErrors(['message' => $data['message']])
                            ->withInput();
            } else {
                return redirect()->back()
                            ->withErrors(['message' => 'BadResponseException: ' . $response->getResponse()->getReasonPhrase()])
                            ->withInput();
            }
        }
        $json = json_decode($response->getBody(), true);
        if ($response->getStatusCode() == 200) {
            $request->session()->flash('flash_message', 'Product deleted successfully.');
        } else {
            return redirect()->back()
                        ->withErrors(['message' => $json['message']])
                        ->withInput();
        }
        return redirect('admin/products');
    }

    private function getBrands($request)
    {
        $token = $request->session()->get('auth.access_token', '');
        $response = $this->httpHelper->get('admins/brands', [], 'Bearer ' . $token);
        if ($response instanceof BadResponseException) {
            return [];
        }
        $json = json_decode($response->getBody(), true);
        if ($response->getStatusCode() == 200) {
            return $json['brands'];
        } else {
            return [];
        }
    }

    private function getCategories($request)
    {
        $token = $request->session()->get('auth.access_token', '');
        $params = [
            'parent_id' => "0",
            'get_children' => 1
        ];
        $response = $this->httpHelper->post('admins/categories/search', $params, 'Bearer ' . $token);
        if ($response instanceof BadResponseException) {
            return [];
        }
        $json = json_decode($response->getBody(), true);
        if ($response->getStatusCode() == 200) {
            return $json['categories'];
        } else {
            return [];
        }
    }
}
