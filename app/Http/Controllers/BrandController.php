<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\BadResponseException;

use DataTables;
use Validator;

class BrandController extends Controller
{
    protected $states = [1 => 'ACTIVE', 2 => 'PENDING', 3 => 'BLOCKED', 4 => 'DELETED'];
    /**
     * Get order page.
     *
     * @param  Illuminate\Http\Request $request
     * @return Illuminate\Http\RedirectResponse
     */
    public function index(Request $request)
    {
        $this->userProfile = $this->getUserProfile($request);
        if (!$this->userProfile) {
            return redirect('auth/logout')->with('errors', 'Not Authorized.');
        }
        $data['user_profile'] = $this->userProfile;

        return view('admin.brands.index', $data);
    }

    public function datatables(Request $request)
    {
        $this->userProfile = $this->getUserProfile($request);
        if (!$this->userProfile) {
            return redirect('auth/logout')->with('errors', 'Not Authorized.');
        }

        $token = $request->session()->get('auth.access_token', '');
        $params = [
            'get_all_state' => 1,
        ];
        $response = $this->httpHelper->post('admins/brands/search', $params, 'Bearer ' . $token);
        if ($response instanceof BadResponseException) {
            return ['error' => true, 'message' => 'BadResponseException: ' . $response->getResponse()->getReasonPhrase()];
        }
        $json = json_decode($response->getBody(), true);
        if ($response->getStatusCode() == 200) {
            $brands = collect($json['brands']);
            // return $brands;
            return DataTables::of($brands)->toJson();
        } else {
            return $json;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->userProfile = $this->getUserProfile($request);
        if (!$this->userProfile) {
            return redirect('auth/logout')->with('errors', 'Not Authorized.');
        }
        $data['states'] = $this->states;
        return view('admin.brands.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:50',
            'description' => 'required|max:255',
            'logo_url' => 'nullable|url|max:255',
            'state' => 'required|in:1,2,3,4',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        $token = $request->session()->get('auth.access_token', '');
        $params = $request->all();
        $response = $this->httpHelper->post('admins/brands/add', $params, 'Bearer ' . $token);
        if ($response instanceof BadResponseException) {
            return redirect()->back()
                        ->withErrors(['message' => 'BadResponseException: ' . $response->getResponse()->getReasonPhrase()])
                        ->withInput();
        }
        $json = json_decode($response->getBody(), true);
        if ($response->getStatusCode() == 201) {
            $request->session()->flash('flash_message', 'Brand created successfully.');
        } else {
            return redirect()->back()
                        ->withErrors(['message' => $json['message']])
                        ->withInput();
        }
        return redirect('admin/brands');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $this->userProfile = $this->getUserProfile($request);
        if (!$this->userProfile) {
            return redirect('auth/logout')->with('errors', 'Not Authorized.');
        }
        $token = $request->session()->get('auth.access_token', '');
        $params = [
            'get_all_state' => 1,
            'brand_id' => $id,
        ];
        $response = $this->httpHelper->post('admins/brands/search', $params, 'Bearer ' . $token);
        if ($response instanceof BadResponseException) {
            $data = json_decode($response->getResponse()->getBody(), true);
            if (!empty($data)) {
                return redirect()->back()
                            ->withErrors(['message' => $data['message']])
                            ->withInput();
            } else {
                return redirect()->back()
                            ->withErrors(['message' => 'BadResponseException: ' . $response->getResponse()->getReasonPhrase()])
                            ->withInput();
            }
        }
        $json = json_decode($response->getBody(), true);
        if ($response->getStatusCode() == 200) {
            $data['detail'] = $json['brands'][0];
        } else {
            return redirect()->back()
                        ->withErrors(['message' => $json['message']])
                        ->withInput();
        }
        $data['states'] = $this->states;
        return view('admin.brands.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'brand_id' => 'required|max:36',
            'name' => 'required|max:50',
            'description' => 'required|max:255',
            'logo_url' => 'nullable|url|max:255',
            'state' => 'required|in:1,2,3,4',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        $token = $request->session()->get('auth.access_token', '');
        $params = $request->all();
        $response = $this->httpHelper->post('admins/brands/update', $params, 'Bearer ' . $token);
        if ($response instanceof BadResponseException) {
            $data = json_decode($response->getResponse()->getBody(), true);
            if (!empty($data)) {
                return redirect()->back()
                            ->withErrors(['message' => $data['message']])
                            ->withInput();
            } else {
                return redirect()->back()
                            ->withErrors(['message' => 'BadResponseException: ' . $response->getResponse()->getReasonPhrase()])
                            ->withInput();
            }
        }
        $json = json_decode($response->getBody(), true);
        if ($response->getStatusCode() == 200) {
            $request->session()->flash('flash_message', 'Brand updated successfully.');
        } else {
            return redirect()->back()
                        ->withErrors(['message' => $json['message']])
                        ->withInput();
        }
        return redirect('admin/brands/' . $params['brand_id'] . '/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'brand_id' => 'required|max:36',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        $token = $request->session()->get('auth.access_token', '');
        $params = $request->all();
        $response = $this->httpHelper->post('admins/brands/delete', $params, 'Bearer ' . $token);
        if ($response instanceof BadResponseException) {
            $data = json_decode($response->getResponse()->getBody(), true);
            if (!empty($data)) {
                return redirect()->back()
                            ->withErrors(['message' => $data['message']])
                            ->withInput();
            } else {
                return redirect()->back()
                            ->withErrors(['message' => 'BadResponseException: ' . $response->getResponse()->getReasonPhrase()])
                            ->withInput();
            }
        }
        $json = json_decode($response->getBody(), true);
        if ($response->getStatusCode() == 200) {
            $request->session()->flash('flash_message', 'Brand deleted successfully.');
        } else {
            return redirect()->back()
                        ->withErrors(['message' => $json['message']])
                        ->withInput();
        }
        return redirect('admin/brands');
    }
}
