<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{

    /**
     * Get order page.
     *
     * @param  Illuminate\Http\Request $request
     * @return Illuminate\Http\RedirectResponse
     */
    public function index(Request $request)
    {
        $this->userProfile = $this->getUserProfile($request);
        if (!$this->userProfile) {
            return redirect('auth/logout')->with('errors', 'Not Authorized.');
        }

        $data['user_profile'] = $this->userProfile;

        return view('admin.dashboard', $data);
    }
}
