<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;

use App\Helpers\HttpHelper;
use GuzzleHttp\Exception\BadResponseException;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $httpHelper;

    public function __construct()
    {
        $this->httpHelper = new HttpHelper();
    }

    /**
     * Get user's profile by token bearer
     *
     * @param  Illuminate\Http\Request $request
     * @return mixed
     */
    protected function getUserProfile(Request $request)
    {
        $token = $request->session()->get('auth.access_token', '');

        $response = $this->httpHelper->get('users/profile', [], 'Bearer ' . $token);
        if ($response instanceof BadResponseException) {
            return false;
        }

        if ($response->getStatusCode() == 200) {
            $json = json_decode($response->getBody(), true);
            if ($json['user']['state'] == 'ACTIVE' && $json['user']['role'] == 1) {
                $request->session()->put('user', $json['user']);
                return $json['user'];
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
