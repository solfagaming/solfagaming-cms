<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\BadResponseException;

use DataTables;
use Validator;

class CategoryController extends Controller
{
    protected $states = [1 => 'ACTIVE', 2 => 'PENDING', 3 => 'BLOCKED', 4 => 'DELETED'];
    /**
     * Get order page.
     *
     * @param  Illuminate\Http\Request $request
     * @return Illuminate\Http\RedirectResponse
     */
    public function index(Request $request)
    {
        $this->userProfile = $this->getUserProfile($request);
        if (!$this->userProfile) {
            return redirect('auth/logout')->with('errors', 'Not Authorized.');
        }
        $data['user_profile'] = $this->userProfile;

        return view('admin.categories.index', $data);
    }

    public function datatables(Request $request)
    {
        $this->userProfile = $this->getUserProfile($request);
        if (!$this->userProfile) {
            return redirect('auth/logout')->with('errors', 'Not Authorized.');
        }

        $token = $request->session()->get('auth.access_token', '');
        $params = [
            'get_all_state' => 1,
            'get_parent' => 1,
        ];
        $response = $this->httpHelper->post('admins/categories/search', $params, 'Bearer ' . $token);
        if ($response instanceof BadResponseException) {
            return ['error' => true, 'message' => 'BadResponseException: ' . $response->getResponse()->getReasonPhrase()];
        }
        $json = json_decode($response->getBody(), true);
        if ($response->getStatusCode() == 200) {
            $categories = collect($json['categories']);
            // return $categories;
            return DataTables::of($categories)->toJson();
        } else {
            return $json;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->userProfile = $this->getUserProfile($request);
        if (!$this->userProfile) {
            return redirect('auth/logout')->with('errors', 'Not Authorized.');
        }
        $data['parent_categories'] = $this->getParentCategories($request);
        $data['states'] = $this->states;
        return view('admin.categories.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'parent_id' => 'required|max:36',
            'name' => 'required|max:50',
            'description' => 'required|max:255',
            'logo_url' => 'nullable|url|max:255',
            'state' => 'required|in:1,2,3,4',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        $token = $request->session()->get('auth.access_token', '');
        $params = $request->all();
        $response = $this->httpHelper->post('admins/categories/add', $params, 'Bearer ' . $token);
        if ($response instanceof BadResponseException) {
            return redirect()->back()
                        ->withErrors(['message' => 'BadResponseException: ' . $response->getResponse()->getReasonPhrase()])
                        ->withInput();
        }
        $json = json_decode($response->getBody(), true);
        if ($response->getStatusCode() == 201) {
            $request->session()->flash('flash_message', 'Category created successfully.');
        } else {
            return redirect()->back()
                        ->withErrors(['message' => $json['message']])
                        ->withInput();
        }
        return redirect('admin/categories');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $this->userProfile = $this->getUserProfile($request);
        if (!$this->userProfile) {
            return redirect('auth/logout')->with('errors', 'Not Authorized.');
        }
        $token = $request->session()->get('auth.access_token', '');
        $params = [
            'get_all_state' => 1,
            'category_id' => $id,
        ];
        $response = $this->httpHelper->post('admins/categories/search', $params, 'Bearer ' . $token);
        if ($response instanceof BadResponseException) {
            $data = json_decode($response->getResponse()->getBody(), true);
            if (!empty($data)) {
                return redirect()->back()
                            ->withErrors(['message' => $data['message']])
                            ->withInput();
            } else {
                return redirect()->back()
                            ->withErrors(['message' => 'BadResponseException: ' . $response->getResponse()->getReasonPhrase()])
                            ->withInput();
            }
        }
        $json = json_decode($response->getBody(), true);
        if ($response->getStatusCode() == 200) {
            $data['detail'] = $json['categories'][0];
        } else {
            return redirect()->back()
                        ->withErrors(['message' => $json['message']])
                        ->withInput();
        }
        $data['parent_categories'] = $this->getParentCategories($request);
        $data['states'] = $this->states;
        return view('admin.categories.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required|max:36',
            'parent_id' => 'required|max:36',
            'name' => 'required|max:50',
            'description' => 'required|max:255',
            'logo_url' => 'nullable|url|max:255',
            'state' => 'required|in:1,2,3,4',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        $token = $request->session()->get('auth.access_token', '');
        $params = $request->all();
        $response = $this->httpHelper->post('admins/categories/update', $params, 'Bearer ' . $token);
        if ($response instanceof BadResponseException) {
            $data = json_decode($response->getResponse()->getBody(), true);
            if (!empty($data)) {
                return redirect()->back()
                            ->withErrors(['message' => $data['message']])
                            ->withInput();
            } else {
                return redirect()->back()
                            ->withErrors(['message' => 'BadResponseException: ' . $response->getResponse()->getReasonPhrase()])
                            ->withInput();
            }
        }
        $json = json_decode($response->getBody(), true);
        if ($response->getStatusCode() == 200) {
            $request->session()->flash('flash_message', 'Category updated successfully.');
        } else {
            return redirect()->back()
                        ->withErrors(['message' => $json['message']])
                        ->withInput();
        }
        return redirect('admin/categories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required|max:36',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        $token = $request->session()->get('auth.access_token', '');
        $params = $request->all();
        $response = $this->httpHelper->post('admins/categories/delete', $params, 'Bearer ' . $token);
        if ($response instanceof BadResponseException) {
            $data = json_decode($response->getResponse()->getBody(), true);
            if (!empty($data)) {
                return redirect()->back()
                            ->withErrors(['message' => $data['message']])
                            ->withInput();
            } else {
                return redirect()->back()
                            ->withErrors(['message' => 'BadResponseException: ' . $response->getResponse()->getReasonPhrase()])
                            ->withInput();
            }
        }
        $json = json_decode($response->getBody(), true);
        if ($response->getStatusCode() == 200) {
            $request->session()->flash('flash_message', 'Category deleted successfully.');
        } else {
            return redirect()->back()
                        ->withErrors(['message' => $json['message']])
                        ->withInput();
        }
        return redirect('admin/categories');
    }


    private function getParentCategories($request)
    {
        $token = $request->session()->get('auth.access_token', '');
        $params = ['parent_id' => 0];
        $response = $this->httpHelper->post('admins/categories/search', $params, 'Bearer ' . $token);
        if ($response instanceof BadResponseException) {
            return ['error' => true, 'message' => 'BadResponseException: ' . $response->getResponse()->getReasonPhrase()];
        }
        $json = json_decode($response->getBody(), true);
        if ($response->getStatusCode() == 200) {
            return $json['categories'];
        } else {
            return $json;
        }
    }
}
