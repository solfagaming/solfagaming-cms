<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Http\Request;
use Validator;

class AuthController extends Controller
{
    /**
     * Logs in user.
     *
     * @param  Illuminate\Http\Request $request
     * @return Illuminate\Http\RedirectResponse
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|min:3|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/')->with('errors', $validator->errors())->withInput();
        }

        $params = [
            'username' => $request->input('email'),
            'password' => $request->input('password'),
            'client_id' => getenv('CLIENT_ID'),
            'client_secret' => getenv('CLIENT_SECRET'),
            'grant_type' => 'password',
            'scope' => ''
        ];
        $response = $this->httpHelper->post('oauth/token', $params);

        if ($response instanceof BadResponseException) {
            $errors = json_decode($response->getResponse()->getBody());
            return back()->with('errors', collect($errors));
        }
        if ($response->getStatusCode() == 200) {
            $json = json_decode($response->getBody(), true);

            $request->session()->put('auth', $json);
            $request->session()->put('logged_in', true);
            $this->getUserProfile($request);
            return redirect('admin/dashboard');
        } else {
            return back()->with('errors', ['Username atau password tidak dikenali.']);
        }
    }

    /**
     * Logs out currently logged in user.
     *
     * @param  Illuminate\Http\Request $request
     * @return Illuminate\Http\RedirectResponse
     */
    public function logout(Request $request)
    {
        $errors = array();
        if (session('errors')) {
            $errors = [
                'message' => session('errors')
            ];
        }
        $request->session()->flush();
        return redirect('/')->with('errors', collect($errors));
    }
}
