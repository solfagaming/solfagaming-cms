$(document).ready(function() {
    if ($('#branddatatable').length){
        var table = $('#branddatatable').DataTable({
            "columns": [
                { data: null, "className": "text-center", "orderable": false, "searchable": false },
                { data: "name" },
                { data: "slug", "searchable": false },
                { data: "description", "orderable": false },
                { data: null, "className": "text-center", "searchable": false },
                { data: null, "className": "text-center", "orderable": false, "searchable": false },
            ],
            "columnDefs": [
                {
                    "targets": 0,
                    "render": function (data, type, full, meta) {
                        return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">' +
                            '<input type="checkbox" class="checkboxes" value="' + data.brand_id + '" \/>' +
                            '<span><\/span>' +
                            '<\/label>';
                    }
                },
                {
                    "targets": 4,
                    "render": function (data, type, full, meta) {
                        var states = [
                            {"text": null},
                            {"text": '<span class="label label-sm label-success">ACTIVE</span>'},
                            {"text": '<span class="label label-sm label-info">PENDING</span>'},
                            {"text": '<span class="label label-sm label-warning">BLOCKED</span>'},
                            {"text": '<span class="label label-sm label-danger">DELETED</span>'}
                        ];
                        return states[data.state].text;
                    }
                },
                {
                    "targets": 5,
                    "render": function (data, type, full, meta) {
                        return '<a href="/admin/brands/' + data.brand_id + '/edit" title="Ubah"><i class="fa fa-pencil"></i>&nbsp;</a>' +
                            '<a href="javascript:void(0)" class="delete" title="Hapus"><i class="fa fa-trash"></i>&nbsp;</a>';
                    },
                }
            ],
            "ajax": "/admin/brands/datatables",
            "serverSide": false,
        });

        $('#branddatatable tbody').on( 'click', '.delete', function () {
            var data = table.row( $(this).parents('tr') ).data();
            $("#modal_delete .itemname").text(data.name);
            $("#modal_delete .brand_id").val(data.brand_id);
            $("#modal_delete").modal("show");
        });
    }
});