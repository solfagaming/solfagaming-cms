$(document).ready(function() {
    if ($('#productdatatable').length){
        var table = $('#productdatatable').DataTable({
            "columns": [
                { data: null, "className": "text-center", "orderable": false, "searchable": false },
                { data: "name" },
                { data: "category.name" },
                { data: "price", "className": "text-right", "searchable": false },
                { data: "stock", "className": "text-center", "searchable": false },
                { data: null, "className": "text-center", "searchable": false },
                { data: null, "className": "text-center", "orderable": false, "searchable": false },
            ],
            "columnDefs": [
                {
                    "targets": 0,
                    "render": function (data, type, full, meta) {
                        return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">' +
                            '<input type="checkbox" class="checkboxes" value="' + data.product_id + '" \/>' +
                            '<span><\/span>' +
                            '<\/label>';
                    }
                },
                {
                    "targets": 5,
                    "render": function (data, type, full, meta) {
                        if (data.state == 'ACTIVE') {
                            return '<span class="label label-sm label-success">ACTIVE</span>';
                        } else if (data.state == 'PENDING') {
                            return '<span class="label label-sm label-info">PENDING</span>';
                        } else if (data.state == 'BLOCKED') {
                            return '<span class="label label-sm label-warning">BLOCKED</span>';
                        } else if (data.state == 'DELETED') {
                            return '<span class="label label-sm label-danger">DELETED</span>';
                        } else {
                            return 'Undefined';
                        }
                    }
                },
                {
                    "targets": 6,
                    "render": function (data, type, full, meta) {
                        return '<a href="/admin/products/' + data.product_id + '/edit" title="Ubah"><i class="fa fa-pencil"></i>&nbsp;</a>' +
                            '<a href="javascript:void(0)" class="delete" title="Hapus"><i class="fa fa-trash"></i>&nbsp;</a>';
                    },
                }
            ],
            "ajax": "/admin/products/datatables",
            "serverSide": false,
        });

        $('#productdatatable tbody').on( 'click', '.delete', function () {
            var data = table.row( $(this).parents('tr') ).data();
            $("#modal_delete .itemname").text(data.name);
            $("#modal_delete .product_id").val(data.product_id);
            $("#modal_delete").modal("show");
        });
    }
    // wysihtml5 on product form
    if ($('textarea.wysihtml5').length) {
        $('textarea.wysihtml5').wysihtml5()
    }
    // kartik-v/bootstrap-fileinput for create product
    if ($('#input-images').length){
        $("#input-images").fileinput({
            maxFileCount: 5,
            allowedFileExtensions: ["jpg", "jpeg", "png", "gif"],
            maxFileSize: 1000,
            browseClass: "btn btn-primary btn-block",
            browseLabel: "Pilih Gambar",
            browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
            showUpload: false,
            showRemove: false,
            showCaption: false
        });
    }
});

// check if form Dropzone exist
if ($('#images-form').length){
    // Dropzone for edit product
    Dropzone.options.imagesForm = {
        maxFiles: 5,
        dictDefaultMessage: "",
        init: function() {
            //get existing image data
            mockFiles = JSON.parse(mockFiles);
            // Capture the Dropzone instance as closure.
            var _this = this;
            $.each(mockFiles, function(index, mockFile) {
                var file = { name: mockFile.photo, size: 1, type: 'image/jpeg' };
                _this.options.addedfile.call(_this, file);
                _this.options.thumbnail.call(_this, file, asset_url + mockFile.photo);
                file.previewElement.classList.add('dz-success');
                file.previewElement.classList.add('dz-complete');

                // ADD REMOVE BUTTON
                var removeButton = Dropzone.createElement("<a href='javascript:;'' class='btn btn-danger btn-sm btn-block' data-id='" + mockFile.uuid + "' data-photo='" + mockFile.photo + "'>Remove</a>");
                // Listen to the click event
                removeButton.addEventListener("click", function(e) {
                  // Make sure the button click doesn't submit the form:
                  e.preventDefault();
                  e.stopPropagation();
                  if (!confirm('Apakah anda yakin ingin menghapus file?')) {
                      return false;
                  }
                  $.ajax({
                      url: '/admin/products/images/' + productId + '/delete',
                      type: 'POST',
                      dataType: 'json',
                      data: {image_uuid: this.dataset.id, photo: this.dataset.photo, _token: $('#images-form input[name=_token]').val()},
                      success: function(jsonObj) {
                          alert(jsonObj.message);
                          // Remove the file preview.
                          _this.removeFile(file);
                      }
                  }).fail(function() {
                    alert( "Failed to delete image." );
                  });
                });
                // Add the button to the file preview element.
                file.previewElement.appendChild(removeButton);

                // ADD RADIO BUTTON
                var checked = (mockFile.primary == 1) ? ' checked' : '';
                var defaultRadioButton = Dropzone.createElement('<div class="mt-radio-list" data-id="' + mockFile.id + '"><label class="mt-radio mt-radio-outline">Primary<input type="radio" name="default_pic" value="' + mockFile.id + '"' + checked + ' /><span></span></label></div>');
                // Listen to the click event
                defaultRadioButton.addEventListener("click", function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    if(this.getElementsByTagName('input')[0].checked != true){
                        var _radio = this;
                        $.ajax({
                            url: '/admin/products/images/' + productId + '/primary',
                            type: 'POST',
                            dataType: 'json',
                            data: {image_uuid: this.dataset.id, _token: $('#images-form input[name=_token]').val()},
                            success: function(jsonObj){
                                alert(jsonObj.message);
                                _radio.getElementsByTagName('input')[0].checked = true;
                            }
                        }).fail(function() {
                          alert( "Failed to set primary image." );
                        });
                    }
                });
                file.previewElement.appendChild(defaultRadioButton);
            });

            this.on("success", function(file, response) {
                // ADD REMOVE BUTTON
                var removeButton = Dropzone.createElement("<a href='javascript:;'' class='btn btn-danger btn-sm btn-block' data-id='" + response.image_id + "' data-photo='" + response.photo + "'>Remove</a>");
                // Capture the Dropzone instance as closure.
                var _this = this;
                // Listen to the click event
                removeButton.addEventListener("click", function(e) {
                    // Make sure the button click doesn't submit the form:
                    e.preventDefault();
                    e.stopPropagation();
                    // confirmation
                    if (!confirm('Apakah anda yakin ingin menghapus file?')) {
                        return false;
                    }
                    // If you want to the delete the file on the server as well,
                    // you can do the AJAX request here.
                    $.ajax({
                        url: '/admin/products/images/' + productId + '/delete',
                        type: 'POST',
                        dataType: 'json',
                        data: {image_uuid: this.dataset.id, photo: this.dataset.photo, _token: $('#images-form input[name=_token]').val()},
                        beforeSend: function(xhr){

                        },
                        success: function(jsonObj) {
                            alert(jsonObj.message);
                            // Remove the file preview.
                            _this.removeFile(file);
                        }
                    }).fail(function() {
                      alert( "Failed to delete image." );
                    });
                });
                // Add the button to the file preview element.
                file.previewElement.appendChild(removeButton);

                // ADD RADIO BUTTON
                var defaultRadioButton = Dropzone.createElement('<div class="mt-radio-list" data-id="' + response.image_id + '"><label class="mt-radio mt-radio-outline">Primary<input type="radio" name="default_pic" value="' + response.image_id + '"/><span></span></label></div>');
                // Listen to the click event
                defaultRadioButton.addEventListener("click", function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    if(this.getElementsByTagName('input')[0].checked != true){
                        var _radio = this;
                        $.ajax({
                            url: '/suppliers/products/images/' + productId + '/primary',
                            type: 'POST',
                            dataType: 'json',
                            data: {image_id: this.dataset.id, _token: $('#images-form input[name=_token]').val()},
                            success: function(jsonObj){
                                alert(jsonObj.message);
                                _radio.getElementsByTagName('input')[0].checked = true;
                            }
                        }).fail(function() {
                          alert( "Failed to set primary image." );
                        });
                    }
                });
                file.previewElement.appendChild(defaultRadioButton);
            });

            this.on("error", function(file, errorMessage) {
                console.log(file, errorMessage);
                alert(errorMessage.message);
            });
        }
    }
}