$(document).ready(function() {
    if ($('#categorydatatable').length){
        var table = $('#categorydatatable').DataTable({
            "columns": [
                { data: null, "className": "text-center", "orderable": false, "searchable": false },
                { data: "name" },
                { data: "description", "orderable": false },
                { data: "parent", "className": "text-center", "searchable": false },
                { data: null, "className": "text-center", "searchable": false },
                { data: null, "className": "text-center", "orderable": false, "searchable": false },
            ],
            "columnDefs": [
                {
                    "targets": 0,
                    "render": function (data, type, full, meta) {
                        return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">' +
                            '<input type="checkbox" class="checkboxes" value="' + data.category_id + '" \/>' +
                            '<span><\/span>' +
                            '<\/label>';
                    }
                },
                {
                    "targets": 3,
                    "render": function (data, type, full, meta) {
                        if (data == null) {
                            return '0';
                        } else {
                            return data.name;
                        }

                    }
                },
                {
                    "targets": 4,
                    "render": function (data, type, full, meta) {
                        var states = [
                            {"text": null},
                            {"text": '<span class="label label-sm label-success">ACTIVE</span>'},
                            {"text": '<span class="label label-sm label-info">PENDING</span>'},
                            {"text": '<span class="label label-sm label-warning">BLOCKED</span>'},
                            {"text": '<span class="label label-sm label-danger">DELETED</span>'}
                        ];
                        return states[data.state].text;
                    }
                },
                {
                    "targets": 5,
                    "render": function (data, type, full, meta) {
                        return '<a href="/admin/categories/' + data.category_id + '/edit" title="Ubah"><i class="fa fa-pencil"></i>&nbsp;</a>' +
                            '<a href="javascript:void(0)" class="delete" title="Hapus"><i class="fa fa-trash"></i>&nbsp;</a>';
                    },
                }
            ],
            "ajax": "/admin/categories/datatables",
            "serverSide": false,
        });

        $('#categorydatatable tbody').on( 'click', '.delete', function () {
            var data = table.row( $(this).parents('tr') ).data();
            $("#modal_delete .itemname").text(data.name);
            $("#modal_delete .category_id").val(data.category_id);
            $("#modal_delete").modal("show");
        });
    }
});